package com.springapp.mvc;

import com.springapp.dao.EventDao;
import com.springapp.dao.UserDao;
import com.springapp.models.Event;
import com.springapp.models.EventType;
import com.springapp.models.User;
import com.springapp.service.EventService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class EventController {

    @Autowired
    private EventDao eventDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private EventService eventService;
    @Autowired
    private ConnectionRepository connectionRepository;

    @RequestMapping(value = "/events", method = RequestMethod.GET)
    public String getEvents(ModelMap model) {
        List<Event> events = eventDao.list();
        model.addAttribute("events", events);
        return "events";
    }

    @RequestMapping(value = "/event", method = RequestMethod.GET)
    public String getEventCreationForm(ModelMap model) {
        model.addAttribute("event", new Event());
        return "createEvent";
    }

    @RequestMapping(value = "/event", method = RequestMethod.POST)
    public String createEvent(@RequestParam("eventType") String eventType, @RequestParam("hashtag") String hashtag, @RequestParam("name") String name,
                              @RequestParam("description") String description, @RequestParam("date") String date, ModelMap model) {

        DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
        Date eventDate = null;
        try {
            eventDate = format.parse(date);
        } catch (ParseException e) {
            return "notFound";
        }
        Event event = new Event(EventType.valueOf(eventType), hashtag, name, description, eventDate);
        model.addAttribute("event", event);
        eventDao.createEvent(event.getEventType(), event.getHashtag(), event.getName(), event.getDescription(), event.getDate());

        return "redirect:/events";
    }

    @RequestMapping(value = "/event/{id}/subscribe", method = RequestMethod.GET)
    public String subscribeToEvent(@PathVariable("id") long id) {
        Optional<Event> optionalEvent = eventDao.find(id);
        String userId = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> optionalUser = userDao.find(userId);
        Event event;
        User user;
        if (optionalEvent.isPresent() && optionalUser.isPresent()) {
            event = optionalEvent.get();
            user = optionalUser.get();
        } else {
            return "notFound";
        }

        if (!event.getParticipants().contains(user)) {
            eventDao.subscribeToEvent(event, user);
        }

        return "redirect:/events";
    }

    @RequestMapping(value = "/event/{id}", method = RequestMethod.GET)
    public String getEvent(@PathVariable("id") long id, ModelMap model) {
        try {
            Optional<Event> optionalEvent = eventDao.find(id);
            Event event;
            event = optionalEvent.get();
            model.addAttribute("event", event);

            Connection<Facebook> connection = connectionRepository.findPrimaryConnection(Facebook.class);
            Facebook facebook = connection.getApi();

            for (User user : event.getParticipants()) {
                PagedList<Post> feed = facebook.feedOperations().getFeed(user.getFacebookId());
                List<Post> posts = eventService.getPostsForEvent(event, feed);

                model.addAttribute("posts", posts);

            }

            return "eventDetails";
        } catch(Exception e) {
            return "notFound";
        }
    }

    @RequestMapping(value = "/event/{id}", method = RequestMethod.POST)
    public String deleteEvent(@PathVariable("id") long id, ModelMap model) {
        Optional<Event> optionalEvent = eventDao.find(id);
        Event event;
        event = optionalEvent.get();
        model.remove(event);

        eventDao.deleteEvent(event);

        return "redirect:/events";
    }

    @RequestMapping(value = "/event/{id}/update", method = RequestMethod.GET)
    public String getEventUpdateForm() {

        return "createEvent";
    }

    @RequestMapping(value = "/event/{id}/update", method = RequestMethod.POST)
    public String updateEvent(@PathVariable("id") long id, @RequestParam("eventType") String eventType, @RequestParam("hashtag") String hashtag, @RequestParam("name") String name,
                              @RequestParam("description") String description, @RequestParam("date") String date, ModelMap model) {

        DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
        Date eventDate = null;
        try {
            eventDate = format.parse(date);
        } catch (ParseException e) {
        }
        Optional<Event> optionalEvent = eventDao.find(id);
        Event event;
        if (optionalEvent.isPresent()) {
            event = optionalEvent.get();
        } else {
            return "notFound";
        }
        model.replace("event", event);
        eventDao.updateEvent(event, EventType.valueOf(eventType), hashtag, name, description, eventDate);

        return "redirect:/events";
    }
}
