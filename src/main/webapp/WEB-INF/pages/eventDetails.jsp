<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Met-A-Life</title>

    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/events">Met-A-Life</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/events">Events</a></li>
                <li class="active"><a href="/event">Create</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>

<div class="container" style="padding-top: 70px;">

    <div>
        ${event.eventType}: <b>${event.name}</b>

        <p>${event.hashtag}</p>

        <p>${event.description}</p>

        <p>${event.date}</p>

        <p><a href="<c:out value="/event/${event.id}/subscribe"/>">Subscribe</a></p>
        <p><form role="form" method="POST"><button type="submit" class="btn btn-default">Delete</button></form>
        <form role="form" action="<c:out value="/event/${event.id}/update"/>"><button type="submit" class="btn btn-default">Update</button></form></p>
    </div>

    <c:forEach var="posts" items="${posts}">
        <div>
            <b>${posts.from.name}</b>

            <p>${posts.message}</p>
        </div>
        <hr/>
    </c:forEach>

</div>
<!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
</body>
</html>