package com.springapp.models;

public enum SystemRole {
    User, Admin
}
