package com.springapp.service;

import com.springapp.models.Event;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;

import java.util.List;

public interface EventService {
    List<Post> getPostsForEvent(Event event, PagedList<Post> feed);
}
