package com.springapp.models;

public enum EventType {
    Conference, Hackathon, Workshop
}
