package com.springapp.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "events")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EVENT_ID", unique = true, nullable = false)
    private long id;

    @Enumerated
    @Column
    private EventType eventType;

    @Column
    private String hashtag;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private Date date;

    @Column
    private Date creationDate;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "event_user", joinColumns = {
            @JoinColumn(name = "EVENT_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "USER_ID", nullable = false, updatable = false)
    })
    private List<User> participants = new ArrayList<>();

    public Event() {
    }

    public Event(EventType eventType, String hashtag, String name, String description, Date date) {
        this.eventType = eventType;
        this.hashtag = hashtag;
        this.name = name;
        this.description = description;
        this.date = date;
        this.creationDate = new Date();
    }

    public long getId() { return id; }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void addParticipant(User participant) {
        this.participants.add(participant);
    }
}
