package com.springapp.service;

import com.springapp.models.Event;
import org.joda.time.DateTime;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Service
public class EventServiceImpl implements EventService {

    @Override
    public List<Post> getPostsForEvent(Event event, PagedList<Post> feed) {
        List<Post> posts = newArrayList();
        for (Post post : feed) {
            DateTime postTime = new DateTime(post.getCreatedTime());
            DateTime eventTime = new DateTime(event.getCreationDate());
            if (eventTime.isBefore(postTime) && post.getMessage().contains(event.getHashtag())) {
                posts.add(post);
            }
        }
        return posts;
    }
}
