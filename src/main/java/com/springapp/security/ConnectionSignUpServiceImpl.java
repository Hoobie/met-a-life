package com.springapp.security;

import com.springapp.dao.UserDao;
import com.springapp.models.SystemRole;
import com.springapp.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ConnectionSignUpServiceImpl implements ConnectionSignUp {

    @Autowired
    private UserDao userDao;

    public String execute(Connection<?> connection) {
        String facebookId = connection.getKey().getProviderUserId();

        // Workaround for in memory user repository
        Optional<User> userOptional = userDao.find(facebookId);

        if (userOptional.isPresent()) {
            return facebookId;
        }
        // end workaround

        UserProfile profile = connection.fetchUserProfile();
        userDao.createUser(SystemRole.User, facebookId, profile.getFirstName(), profile.getLastName());

        return facebookId;
    }
}
