package com.springapp.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;

public class SignInAdapterImpl implements SignInAdapter {

    @Override
    public String signIn(String localUserId, Connection<?> connection, NativeWebRequest request) {
        // TODO: add GrantedAuthority?
        String facebookId = connection.getKey().getProviderUserId();
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(facebookId, null, null));
        return null;
    }
}
