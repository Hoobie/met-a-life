package com.springapp.dao;

import com.springapp.models.Event;
import com.springapp.models.EventType;
import com.springapp.models.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public class EventDaoImpl implements EventDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void createEvent(EventType eventType, String hashtag, String name, String description, Date date) {
        sessionFactory.getCurrentSession().save(new Event(eventType, hashtag, name, description, date));
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional
    public List<Event> list() {
        return (List<Event>) sessionFactory.getCurrentSession().createCriteria(Event.class).list();
    }

    @Override
    @Transactional
    public Optional<Event> find(long id) {
        return Optional.of((Event) sessionFactory.getCurrentSession().get(Event.class, id));
    }

    @Override
    @Transactional
    public void updateEvent(Event event, EventType eventType, String hashtag, String name, String description, Date date) {
        event.setEventType(eventType);
        event.setHashtag(hashtag);
        event.setName(name);
        event.setDescription(description);
        event.setDate(date);

        sessionFactory.getCurrentSession().update(event);
    }

    @Override
    @Transactional
    public void subscribeToEvent(Event event, User user) {
        event.addParticipant(user);
        sessionFactory.getCurrentSession().update(event);
	}

	@Override
    @Transactional
    public void deleteEvent(Event event) {
        sessionFactory.getCurrentSession().delete(event);
    }
}
