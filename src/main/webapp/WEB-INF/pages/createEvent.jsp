<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Event creation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/events">Met-A-Life</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/events">Events</a></li>
                <li class="active"><a href="/event">Create</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>

<div class="container">
    <h2>Create a new event</h2>
    <form role="form" method="POST">
        <div class="form-group">
            <label for="eventType">Event type:</label>
                <select id="eventType" path="eventType" name="eventType" class="form-control">
                    <option value="">Choose a type</option>
                    <option value="Conference">Conference</option>
                    <option value="Hackathon">Hackathon</option>
                    <option value="Workshop">Workshop</option>
                </select>

        </div>
        <div class="form-group">
            <label for="hashtag">Choose a hashtag to bind to your event:</label>
            <input type="text" path="hashtag" name="hashtag" class="form-control" id="hashtag" placeholder="Enter hashtag">
        </div>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" path="name" name="name" class="form-control" id="name" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" path="description" name="description" class="form-control" id="description" placeholder="Describe your event">
        </div>
        <div class="form-group">
            <label for="date">Date of the event:</label>
            <input type="date" path="date" name="date" class="form-control" id="date" placeholder="Choose a date">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
