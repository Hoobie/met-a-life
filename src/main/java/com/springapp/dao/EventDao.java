package com.springapp.dao;

import com.springapp.models.Event;
import com.springapp.models.EventType;
import com.springapp.models.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface EventDao {

    /**
     * Method for creating a new Event.
     *
     * @param eventType
     * @param hashtag
     * @param name
     * @param description
     * @param date
     */
    void createEvent(EventType eventType, String hashtag, String name, String description, Date date);

    List<Event> list();

    /**
     * Method for finding an event by id.
     *
     * @param id
     * @return Event (if found)
     */
    Optional<Event> find(long id);

    /**
     * Method for updating an event information.
     *
     * @param event
     * @param eventType
     * @param hashtag
     * @param name
     * @param description
     * @param date
     */
    void updateEvent(Event event, EventType eventType, String hashtag, String name, String description, Date date);

    void subscribeToEvent(Event event, User user);

    void deleteEvent(Event event);
}
