package com.springapp.dao;

import com.springapp.models.SystemRole;
import com.springapp.models.User;

import java.util.Optional;


public interface UserDao {

    Optional<User> find(long id);

    Optional<User> find(String facebookId);

    void createUser(SystemRole systemRole, String facebookId, String firstName, String lastName);
}
