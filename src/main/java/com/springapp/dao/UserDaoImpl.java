package com.springapp.dao;

import com.springapp.models.SystemRole;
import com.springapp.models.User;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public Optional<User> find(long id) {
        return Optional.ofNullable((User) sessionFactory.getCurrentSession().get(User.class, id));
    }

    @Override
    @Transactional
    public Optional<User> find(String facebookId) {
        return Optional.ofNullable((User) sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("facebookId", facebookId)).uniqueResult());
    }

    @Override
    @Transactional
    public void createUser(SystemRole systemRole, String facebookId, String firstName, String lastName) {
        sessionFactory.getCurrentSession().save(new User(systemRole, facebookId, firstName, lastName));
    }
}
